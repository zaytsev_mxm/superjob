(function () {
  'use strict';

  angular.module('app').controller('ProfileDetailsCtrl', ProfileDetailsCtrl);

  ProfileDetailsCtrl.$inject = ['ProfileSrvc', '$stateParams', 'ProfileUtils'];

  function ProfileDetailsCtrl(ProfileSrvc, $stateParams, ProfileUtils) {
    var vm = this;
    
    vm.profile = null;
    
    vm.toggleFavourites = toggleFavourites;
    vm.purchaseContacts = purchaseContacts;
    
    // Functions
    function toggleFavourites(id){
      ProfileUtils.toggleProfileFieldValue(id, 'in_favourites');
    }
    
    function purchaseContacts(id){
      ProfileUtils.toggleProfileFieldValue(id, 'contacts_purchased');
    }
    
    // Get data
    ProfileSrvc.GetProfile($stateParams.id).then(function(response){
      vm.profile = response;
    });
  }
})();