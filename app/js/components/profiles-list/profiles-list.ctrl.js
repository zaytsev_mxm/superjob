(function () {
  'use strict';

  angular.module('app').controller('ProfilesListCtrl', ProfilesListCtrl);

  ProfilesListCtrl.$inject = ['ProfileSrvc', '$log', 'ProfileUtils'];

  function ProfilesListCtrl(ProfileSrvc, $log, ProfileUtils) {
    var vm = this;
    
    vm.profiles = [];
    
    vm.toggleFavourites = toggleFavourites;
    vm.purchaseContacts = purchaseContacts;
    
    // Functions
    function toggleFavourites(id){
      ProfileUtils.toggleProfileFieldValue(id, 'in_favourites');
    }
    
    function purchaseContacts(id){
      ProfileUtils.toggleProfileFieldValue(id, 'contacts_purchased');
    }
    
    // Get data
    ProfileSrvc.GetProfiles().then(function(response){      
      vm.profiles = response;
    });
  }
})();