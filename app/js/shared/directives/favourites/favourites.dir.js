(function(){
  'use strict';
  
  angular.module('app').directive('favourites', favourites);
  
  favourites.$inject = [];
  
  function favourites(){
    return {
      restrict: 'E',
      replace: true,
      templateUrl: './app/js/shared/directives/favourites/favourites.tpl.html',
      scope: {
        toggleFavourites: '=',
        profile: '='
      },
      link: function($scope, $elem, attrs) {        
        var btn = angular.element($elem[0].querySelector('a'));
        
        btn.on('click', function(){
          $scope.toggleFavourites($scope.profile.id);
        });
      }
    }
  }
})();