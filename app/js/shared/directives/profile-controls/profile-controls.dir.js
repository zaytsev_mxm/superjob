(function(){
  'use strict';
  
  angular.module('app').directive('profileControls', profileControls);
  
  profileControls.$inject = [];
  
  function profileControls(){
    return {
      restrict: 'E',
      replace: true,
      templateUrl: './app/js/shared/directives/profile-controls/profile-controls.tpl.html',
      scope: {
        purchaseContacts: '=',
        profile: '='
      },
      link: function($scope, $elem, attrs) {        
        var purchaseContactsBtn = angular.element($elem[0].querySelector('.purchase-contacts'));
        
        purchaseContactsBtn.on('click', function(){
          $scope.purchaseContacts($scope.profile.id);
        });
      }
    }
  }
})();