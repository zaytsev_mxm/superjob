(function () {
  'use strict';

  angular.module('app').factory('ProfileUtils', ProfileUtils);

  ProfileUtils.$inject = ['ProfileSrvc'];

  function ProfileUtils(ProfileSrvc) {
    var factory = {};
    
    factory.toggleProfileFieldValue = toggleProfileFieldValue;
    
    return factory;
    
    function toggleProfileFieldValue(id, field) {
      ProfileSrvc.GetProfile(id).then(function (response) {
        var props = {};

        props[field] = !response[field];

        ProfileSrvc.UpdateProfile(id, props);
      });
    }
  }
})();