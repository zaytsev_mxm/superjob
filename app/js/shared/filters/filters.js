(function () {
  'use strict';

  function declOfNum(number, titles) {
    var cases = [2, 0, 1, 1, 1, 2];
    return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
  }

  angular.module('customFilters', [])
    .filter('ages', function () {
      return function (input, search) {
        return input + ' ' + declOfNum(input, ['год', 'года', 'лет']);
      }
    })
    .filter('experience', function(){
      return function(input, search){
        var dateNow = Date.now(),
            diff = dateNow - input,
            msInM = 1000 * 60 * 60 * 24 * 30,
            totalMonths = Math.floor(diff / msInM),
            years = Math.floor(totalMonths / 12),
            months = Math.floor(totalMonths % 12),
            result = '';
        
        if (years >= 1) {
          result += years + declOfNum(years, [' год', ' года', ' лет']);
          
          if (months > 0) {
            result += ' ' + months + declOfNum(months, [' месяц', ' месяца', ' месяцев']);
          }
        } else {
          result = months + declOfNum(months, [' месяц', ' месяца', ' месяцев']);
        }
        
        return result;
      }
    });
})();