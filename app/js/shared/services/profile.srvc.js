(function(){
  'use strict';
  
  angular.module('app').service('ProfileSrvc', ProfileSrvc);
  
  ProfileSrvc.$inject = ['$resource', '$q'];
  
  function ProfileSrvc($resource, $q){
    var service = {};
    
    var cachedProfiles = [];
    
    if (localStorage.getItem('cachedProfiles')) {
      cachedProfiles = JSON.parse(localStorage.getItem('cachedProfiles'));
    }
    
    service.GetProfiles = GetProfiles;
    service.GetProfile = GetProfile;
    service.UpdateProfile = UpdateProfile;
    
    return service;
    
    function GetProfiles(){
      var deferred = $q.defer();

      if (cachedProfiles.length) {
        deferred.resolve(cachedProfiles);
      } else {
        $resource('./app/data/profiles.json').query().$promise.then(function(response){
          localStorage.setItem('cachedProfiles', JSON.stringify(response));
          cachedProfiles = response;
          deferred.resolve(response);
        }).catch(function(reason){
          deferred.reject(reason);
        });
      }
      
      return deferred.promise;
    }
    
    function GetProfile(id){
      var deffered = $q.defer();
      
      if (cachedProfiles.length) {
        deffered.resolve(cachedProfiles.find(function(item){
          return item.id == id;
        }));
      } else {
        GetProfiles().then(function(){
          return GetProfile(id).then(function(response){
            deffered.resolve(response);
          });
        }).catch(function(reason){
          deffered.reject(reason);
        });
      }
      
      return deffered.promise;
    }
    
    function UpdateProfile(id, props){
      var deffered = $q.defer();
      
      if (cachedProfiles.length) {
        GetProfile(id).then(function(profile){          
          for (var key in props) {
            profile[key] = props[key];
          }
          
          localStorage.setItem('cachedProfiles', JSON.stringify(cachedProfiles));
        });
      } else {
        GetProfiles().then(function(response){
          return UpdateProfile(id, props).then(function(response){
            deffered.resolve(response);
          });
        }).catch(function(reason){
          deffered.reject(reason);
        });
      }
      
      return deffered.promise;
    }
  }
})();