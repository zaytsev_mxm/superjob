(function () {
  'use strict';

  angular.module('app').config(['$stateProvider', '$urlRouterProvider', 
  function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');
    
    $stateProvider
      .state('home', {
        url: '/',
        views: {
          'content': {
            templateUrl: 'app/js/components/profiles-list/profiles-list.tpl.html',
            controller: 'ProfilesListCtrl as vm'
          },
          'sidebar': {
            templateUrl: 'app/js/components/sidebar/sidebar.tpl.html'
          }
        }
      })
      .state('profile', {
        url: '/profile/:id',
        views: {
          'content': {
            templateUrl: 'app/js/components/profile-details/profile-details.tpl.html',
            controller: 'ProfileDetailsCtrl as vm'
          },
          'sidebar': {
            templateUrl: 'app/js/components/sidebar/sidebar.tpl.html'
          }
        }
      });
    }]);
})();