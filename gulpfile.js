// Gulp plugins
var gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    less = require('gulp-less'),
    autoprefixer = require('gulp-autoprefixer'),
    cssmin = require('gulp-cssmin');

// Variables
var appScripts = './app/js/**/*.js',
    appLess = './app/less/style.less',
    appLessSources = './app/less/**/*.less',
    destDir = './dist/';

// Default task
gulp.task('default', ['scripts', 'styles', 'watch']);

// Scripts
gulp.task('scripts', function() {
  return gulp.src([appScripts])
    .pipe(concat('app.js'))
    .pipe(uglify())
    .pipe(gulp.dest(destDir));
});


// Styles
gulp.task('styles', function(){
  return gulp.src([appLess])
    .pipe(less('style.css'))
    .pipe(autoprefixer())
    .pipe(cssmin())
    .pipe(gulp.dest(destDir));
});

// Watch
gulp.task('watch', function(){
  gulp.watch(appScripts, ['scripts']);
  gulp.watch(appLessSources, ['styles']);
});